Rolljak web app

Overview:
The project involves the automation testing of the registration and login feature of the Rolljak web application. Rolljak is a gamified learning platform to boosts engagement and learning outcomes. The registration and login feature is a critical component of the application, allowing users to create accounts, sign in, and access their learning platform tools.

Goals:
-Automate the testing of the registration and login feature of the Rolljak web application using Selenium WebDriver with Java.
-Ensure the feature functions correctly and securely by covering both positive and negative test scenarios.

Test Environment:
-Selenium WebDriver with Java.
-Google Chrome browser.
-JUnit or TestNG for test case management.
-An integrated development environment (IDE) like Eclipse or IntelliJ.

Automated Test Case:
-Registration
Negative Test Case
-User should be disable to register with blank email and password.
-User should be disable to register with valid email but blank password.
-User should be disable to register with blank email but valid password.
-User should be disable to register with invalid email but valid password.
-User should be disable to register with valid email but invalid password.
-User should be disable to register with registered email and valid password.
Positive Test Case
-User should be able to register with valid unregistered email and valid password.

-Login
Negative Test Case
-User should be disable to login with blank email and password.
-User should be disable to login with registered email but blank password.
-User should be disable to login with registered email but invalid password.
-User should be disable to login with unregistered email and password.
Positive Test Case
-User should be able to login with registered email and valid password.

Automation Testing Workflow:
Set up the test environment.
Create test cases using Selenium WebDriver and Java.
Execute test cases using the Google Chrome browser.
Validate test results and generate reports.
Identify and report defects.
