package app.Rolljak.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

/**
 * used to locate element and handle action on login pages
 */

public class LoginPage {
    WebDriver webDriver;

    // Constructor that takes a WebDriver instance
    public LoginPage(WebDriver driver){
        this.webDriver = driver;
        webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        PageFactory.initElements(webDriver, this);
    }

    //To locate element of title text on login screen
    @FindBy(className = "text-center")
    private WebElement welcomeLoginMessage;

    //To locate element of Google button
    @FindBy(css = "img.oauth-img[src='https://d9kii3f1l5fcf.cloudfront.net/images/googlelogo.png']")
    private WebElement googleLogo;

    //To locate element of Linkedin button
    @FindBy(css = "img.oauth-img[src='https://d9kii3f1l5fcf.cloudfront.net/images/linkedinlogo.png']")
    private WebElement linkedInLogo;

    //To locate element of Facebook logo
    @FindBy(css = "img.oauth-img[src='https://d9kii3f1l5fcf.cloudfront.net/images/facebooklogo.png']")
    private WebElement facebookLogo;

    //To locate element of email input field
    @FindBy(id = "user_email")
    private WebElement emailInput;

    //To locate element of password input field
    @FindBy(id = "user_password")
    private WebElement passwordInput;

    //To locate element of login button
    @FindBy(css = "input[type='submit'][value='LOG IN']")
    private WebElement loginButton;

    //To locate element of sign up link
    @FindBy(linkText = "Sign Up")
    private WebElement signUpLink;

    //To locate element of joining a session link
    @FindBy(partialLinkText = "Click here")
    private WebElement joiningASessionLink;

    @FindBy(css = "div.alert.alert-dismissible.fade.show.alert-danger.mb-3.p-3.rounded.d-flex.align-items-start.gap-2 p.red-text.font-regular")
    private WebElement errorMessageInvalidCredentials;


    //used to get title message text
    public WebElement getWelcomeLoginMessage() {
        return welcomeLoginMessage;
    }

    //used to get error message when login with Invalid Credentials
    public WebElement getErrorMessageInvalidCredentials() {
        return errorMessageInvalidCredentials;
    }

    //used to click login by google
    public void tapGoogle(){
        googleLogo.click();
    }

    //used to click login by LinkedIn
    public void tapLinkedIn(){
        linkedInLogo.click();
    }

    //used to click login by Facebook
    public void tapFacebook(){
        facebookLogo.click();
    }

    //used to input login credentials
    public void inputLoginCredentials(String email, String password){
        emailInput.sendKeys(email);
        passwordInput.sendKeys(password);
    }

    //used to clear email and password input field
    public void clearLoginCredentials(){
        emailInput.clear();
        passwordInput.clear();
    }


    //used to click Login button
    public void tapLogin(){
        loginButton.click();
    }

    //used to click Sign Up link
    public void tapSignUP(){
        signUpLink.click();
    }

    //used to click Joining A Session link
    public void tapJoinSession(){
        joiningASessionLink.click();
    }

}
