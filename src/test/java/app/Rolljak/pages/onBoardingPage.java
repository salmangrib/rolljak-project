package app.Rolljak.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

public class onBoardingPage {

    WebDriver webDriver;

    // Constructor that takes a WebDriver instance
    public onBoardingPage(WebDriver driver){
        this.webDriver = driver;
        webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        PageFactory.initElements(webDriver, this);
    }

    //To locate element of title text
    @FindBy(tagName = "h5")
    private WebElement verifyEmailTitle;

    //To locate element of button to proceed
    @FindBy(className = "verify-email-confirm-btn")
    private WebElement proceedToSetUpLink;



    public WebElement getVerifyEmailTitle(){
        return verifyEmailTitle;
    }

    public WebElement getProceedToSetUpLink() {
        return proceedToSetUpLink;
    }
}
