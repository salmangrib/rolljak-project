package app.Rolljak.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

public class RegisterPage {
    WebDriver webDriver;

    // Constructor that takes a WebDriver instance
    public RegisterPage(WebDriver driver){
        this.webDriver = driver;
        webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        PageFactory.initElements(webDriver, this);
    }

    //To locate element of title text on registration screen
    @FindBy(className = "text-center")
    private WebElement welcomeRegisterMessage;

    //To locate element of Google button
    @FindBy(css = "img.oauth-img[src='https://d9kii3f1l5fcf.cloudfront.net/images/googlelogo.png']")
    private WebElement googleLogo;

    //To locate element of Linkedin button
    @FindBy(css = "img.oauth-img[src='https://d9kii3f1l5fcf.cloudfront.net/images/linkedinlogo.png']")
    private WebElement linkedInLogo;

    //To locate element of Facebook logo
    @FindBy(css = "img.oauth-img[src='https://d9kii3f1l5fcf.cloudfront.net/images/facebooklogo.png']")
    private WebElement facebookLogo;

    //To locate element of email input field
    @FindBy(id = "user_email")
    private WebElement emailInput;

    //To locate element of password input field
    @FindBy(name = "user[password]")
    private WebElement passwordInput;

    //To locate element of sign up button
    @FindBy(id = "rollSignupBtn")
    private WebElement signUpButton;

    //To locate element of login link
    @FindBy(linkText = "Log In")
    private WebElement loginLink;

    //To locate element of joining a session link
    @FindBy(linkText = "Click here")
    private WebElement joiningASessionLink;

    //To locate element of error message when register with invalid email
    @FindBy(className = "invalid-feedback")
    private WebElement errorMessageInvalidEmailMessage;

    //To locate element of error message when register with invalid password
    @FindBy(id = "password-validity")
    private WebElement errorMessageInvalidPasswordMessage;

    //To locate element of error message when register with registered email
    @FindBy(className = "invalid-feedback")
    private WebElement errorMessageRegisteredEmailMessage;

    //used to get title message text
    public WebElement getWelcomeRegisterMessage() {
        return welcomeRegisterMessage;
    }

    //used to get sign up button
    public WebElement getSignUpButton() {
        return signUpButton;
    }

    //used to get error message when register with Invalid Email
    public WebElement getErrorMessageInvalidEmailMessage() {
        return errorMessageInvalidEmailMessage;
    }

    //used to get error message when register with Invalid Password
    public WebElement getErrorMessageInvalidPasswordMessage() {
        return errorMessageInvalidPasswordMessage;
    }

    //used to get error message when register with registered Email
    public WebElement getErrorMessageRegisteredEmailMessage() {
        return errorMessageRegisteredEmailMessage;
    }


    //used to click login by google
    public void tapGoogle(){
        googleLogo.click();
    }

    //used to click login by LinkedIn
    public void tapLinkedIn(){
        linkedInLogo.click();
    }

    //used to click login by Facebook
    public void tapFacebook(){
        facebookLogo.click();
    }

    //used to input registration data
    public void inputRegistrationCredentials(String email, String password){
        emailInput.sendKeys(email);
        passwordInput.sendKeys(password);
    }

    //used to clear email and password input field
    public void clearRegistrationCredentials(){
        emailInput.clear();
        passwordInput.clear();
    }

    //used to click Sign Up button
    public void tapSignUp(){
        signUpButton.click();
    }

    //used to click Login link
    public void tapLogin(){
        loginLink.click();
    }

    //used to click Joining A Session link
    public void tapJoinSession(){
        joiningASessionLink.click();
    }

}

