package app.Rolljak.handler;
import com.github.javafaker.Faker; // Import the Faker library

/**
 * used to provide test data
 */

public class TestDataProvider {
    // Create a Faker instance for generating random data
    private static final Faker faker = new Faker();

    /**
     * Generate and return a random username.
     */
    public static String getRandomUserName() {
        return faker.name().username();
    }
    /**
     * Generate and return a random first name.
     */
    public static String getRandomFirstName() {
        return faker.name().firstName();
    }

    /**
     * Generate and return a random last name.
     */
    public static String getRandomLastName() {
        return faker.name().lastName();
    }

    /**
     * Generate and return a random password that matches the pattern "[A-Z]\\d{10}".
     */
    public static String getRandomPassword() {
        return getRandomStringMatchingPattern("[A-Z]\\d{10}");
    }

    /**
     * Generate and return a random string that matches the provided pattern.
     */
    public static String getRandomStringMatchingPattern(String pattern) {
        return faker.regexify(pattern);
    }

    /**
     * Generate and return a random email address with a specific pattern.
     */
    public static String getRandomEmail() {
        return getRandomStringMatchingPattern("random\\d{8}") + ".rolljak@getnada.com";
    }

    /**
     * Generate and return a random number with a specified number of digits.
     */
    public static String getRandomNumber(int digits) {
        return faker.number().digits(digits);
    }

    /**
     * Generate and return a random phone number starting with "081".
     */
    public static String getRandomPhoneNo() {
        return "081" + getRandomNumber(10);
    }

    /**
     * Generate and return a random password with that < 8 characters.
     */
    public static String getRandomInvalidPassword() {
        return "Test" + getRandomNumber(3);
    }


    /**
     * Generate and return a random city name.
     */
    public static String getRandomCity() {
        return faker.address().city();
    }

    /**
     * Generate and return a random street address.
     */
    public static String getRandomAddress() {
        return faker.address().streetAddress();
    }

    /**
     * Generate and return a random postal code.
     */
    public static String getRandomPostalCode() {
        return faker.address().zipCode();
    }

}
