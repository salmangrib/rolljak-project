package app.Rolljak.handler;

import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Alert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

/**
 * used to handle action
 */

public class Action {
    WebDriver webDriver;

    // Constructor that takes a WebDriver instance
    public Action(WebDriver driver){
        this.webDriver = driver;
    }

    /**
     * Validates whether a WebElement is both visible and enabled.
     *
     * @param webElement The WebElement to be validated.
     */
    public void validateElementIsVisibleAndEnabled(WebElement webElement) {
        try {
            // Create a WebDriverWait instance with a maximum wait time of 5 seconds
            WebDriverWait wait = new WebDriverWait(webDriver, Duration.ofSeconds(5));

            // Wait for the WebElement to be visible
            wait.until(ExpectedConditions.visibilityOf(webElement));

            // Check if the WebElement is displayed and enabled
            if (!webElement.isDisplayed() || !webElement.isEnabled()) {
                System.out.println("Element is not visible or enabled.");
                throw new ElementNotInteractableException(webElement.toString());
            }
        } catch (Exception e) {
            // If any exceptions occur during execution, print the stack trace for debugging
            e.printStackTrace();
        }
    }

    public Action waitElementToBeDisplayed(WebElement element, int timeout) {
        try {
            // Convert the 'timeout' parameter to a Duration object
            Duration duration = Duration.ofSeconds(timeout);

            // Create a WebDriverWait instance with the specified timeout
            WebDriverWait wait = new WebDriverWait(webDriver, Duration.ofSeconds(5));

            // Wait for the WebElement to be visible
            wait.until(ExpectedConditions.visibilityOf(element));
        } catch (Exception e) {
            // If any exceptions occur during execution, print the stack trace for debugging
            e.printStackTrace();
        }
        return this; // Return the WebElementHandler instance to allow for method chaining
    }

    /**
     * Handles browser pop-up alerts, either accepting or dismissing them.
     *
     * @param isApproved A boolean indicating whether to accept (true) or dismiss (false) the alert.
     */

    public void alertHandler(boolean isApproved) {
        try {
            // Convert the 'timeout' parameter to a Duration object
            Duration duration = Duration.ofSeconds(5);
            // Create a WebDriverWait instance with a maximum wait time of 10 seconds
            WebDriverWait wait = new WebDriverWait(webDriver, duration);

            if (isApproved) {
                // If 'isApproved' is true, wait for the alert to be present
                wait.until(ExpectedConditions.alertIsPresent());

                // Switch focus to the alert
                Alert alert = webDriver.switchTo().alert();

                // Accept (ok) the alert
                alert.accept();
            } else {
                // If 'isApproved' is false, wait for the alert to be present
                wait.until(ExpectedConditions.alertIsPresent());

                // Switch focus to the alert
                Alert alert = webDriver.switchTo().alert();

                // Dismiss (cancel) the alert
                alert.dismiss();
            }
        } catch (Exception e) {
            // If any exceptions occur during the execution, print the stack trace for debugging
            e.printStackTrace();
        }
    }

    /**
     * Waits for an alert to appear, retrieves the alert message, and accepts it.
     *
     * @return The text message from the alert.
     */

    public String getMessageAlert(){
        String message = "";
        try {
            // Convert the 'timeout' parameter to a Duration object
            Duration duration = Duration.ofSeconds(5);

            // Create a WebDriverWait instance with a maximum wait time of 10 seconds
            WebDriverWait wait = new WebDriverWait(webDriver, duration);

            // Wait for the alert to be present
            wait.until(ExpectedConditions.alertIsPresent());

            // Use JavaScript to retrieve the alert message
            JavascriptExecutor js = (JavascriptExecutor) webDriver;
            message = js.executeScript("return window.alert.message").toString();

            // Accept the alert (close it)
            js.executeScript("window.alert.ok()");

        } catch (Exception e) {
            // If any exceptions occur during execution, print the stack trace for debugging
            e.printStackTrace();
        }
        // Return the alert message as a string
        return message;
    }
}
