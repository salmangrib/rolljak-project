package app.Rolljak.tests;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import app.Rolljak.handler.Action;
import app.Rolljak.handler.TestDataProvider;
import app.Rolljak.pages.LoginPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

/**
 * Used to test login function, contains of 2 test suite:
 * First test suite contains 4 negative test cases:
 * -User should be disable to login with blank email and password
 * -User should be disable to login with registered email but blank password
 * -User should be disable to login with registered email but invalid password
 * -User should be disable to login with unregistered email and password
 * Second test suite contains 1 positive test cases:
 * -User should be able to login with registered email and valid password
 */

public class TestLogin {

    WebDriver webDriver;
    Action action;

    //used to handle before the test
    @BeforeMethod(alwaysRun = true)
    public void launchChrome(){
        // Used to open the browser
        WebDriverManager.chromedriver().setup();
        System.setProperty("webdriver.chrome.driver", "D:\\QA\\testChrome\\114\\chromedriver-win64\\chromedriver.exe");

        // Declare and initialize ChromeOptions
        ChromeOptions options = new ChromeOptions();
        options.setBinary("D:\\QA\\testChrome\\114\\chrome-win64\\chrome.exe");

        // Used to direct to the web page
        webDriver = new ChromeDriver(options);
        webDriver.get("https://app.rolljak.com");
        webDriver.manage().window().fullscreen();
        webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
    }

    //used to handle after the test
    @AfterMethod
    public void closeDriver(){
        if(webDriver != null){
            webDriver.close();
            System.out.println("Chrome Driver Closed");
        }
    }

    //used to do the test negative scenario
    @Test(description = "user should see error message with different invalid inputs")
    public void userCantLoginWithInvalidCredentials(){

        //used to create an instance of the LoginPage class to interact with the web page and perform login actions
        LoginPage loginPage = new LoginPage(webDriver);

        //used to ensure the welcome message is displayed and has the expected text
        WebElement loginMessageElement = loginPage.getWelcomeLoginMessage();
        assertWelcomeMessage(loginMessageElement);

        /**
         * Used to get invalid credential data
         */
        //used to get registered email
        String registeredEmail = "salman.magribie.qa@gmail.com";

        //used to get random password
        String randomPassword = TestDataProvider.getRandomPassword();

        //used to get random unregistered email
        String unregisteredEmail = TestDataProvider.getRandomEmail();

        /**
         * used to input invalid credential data and do login
         */
        //User should be disable to login with blank email and password
        testInvalidLogin(loginPage, "", "");

        //User should be disable to login with registered email but blank password
        testInvalidLogin(loginPage, registeredEmail, "");

        //User should be disable to login with registered email but invalid password
        testInvalidLogin(loginPage, registeredEmail, randomPassword);

        //User should be disable to login with unregistered email and password
        testInvalidLogin(loginPage, unregisteredEmail, randomPassword);
    }

    //used to assert the welcome message
    private void assertWelcomeMessage(WebElement loginMessageElement) {
        Assert.assertTrue(loginMessageElement.isDisplayed(), "Login message is displayed");
        String expectedLoginMessage = "Welcome Back!";
        String actualLoginMessage = loginMessageElement.getText();
        Assert.assertEquals(actualLoginMessage, expectedLoginMessage, "Login message text is as expected");
    }

    //used to test invalid login
    private void testInvalidLogin(LoginPage loginPage, String email, String password) {
        loginPage.clearLoginCredentials();
        loginPage.inputLoginCredentials(email, password);
        loginPage.tapLogin();

        //retrieve the error message element
        WebDriverWait wait = new WebDriverWait(webDriver, Duration.ofSeconds(5));
        WebElement errorMessageElement = loginPage.getErrorMessageInvalidCredentials();

        //used to assert that the error message is displayed and contains the expected text
        Assert.assertTrue(errorMessageElement.isDisplayed(), "Error message is displayed");
        String expectedErrorMessage = "Invalid Email or password. Please try again.";
        String actualErrorMessage = errorMessageElement.getText();
        Assert.assertEquals(actualErrorMessage, expectedErrorMessage, "Error message text is as expected");
    }


    //used to do the test positive scenario
    @Test(description = "user should be able to login with registered email and valid password")
    public void userCanLoginWithValidCredentials() {
        LoginPage loginPage = new LoginPage(webDriver);

        /**
         * Used to get valid credential data
         */
        String validEmail = "salman.magribie.qa@gmail.com";
        String validPassword = "Admin123";

        /**
         * used to input valid credential data and do login
         */
        loginPage.inputLoginCredentials(validEmail, validPassword);
        loginPage.tapLogin();

        // Assert that the user is successfully logged in to Rolljak user dashboard
        String currentUrl = webDriver.getCurrentUrl();
        Assert.assertTrue(currentUrl.contains("/home"), "User is on the dashboard after login");
    }

}
