package app.Rolljak.tests;

import app.Rolljak.handler.Action;
import app.Rolljak.handler.TestDataProvider;
import app.Rolljak.pages.RegisterPage;
import app.Rolljak.pages.onBoardingPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

/**
 * Used to test registration function, contains of 2 test suite:
 * First test suite contains 6 negative test cases:
 * -User should be disable to register with blank email and password
 * -User should be disable to register with valid email but blank password
 * -User should be disable to register with blank email but valid password
 * -User should be disable to register with invalid email but valid password
 * -User should be disable to register with valid email but invalid password
 * -User should be disable to register with registered email and valid password
 * Second test suite contains 1 positive test cases:
 * -Bot should be disable to register with valid unregistered email and valid password caused protected on recaptcha
 */


public class TestRegister {

    WebDriver webDriver;
    Action action;

    //used to handle before the test
    @BeforeMethod(alwaysRun = true)
    public void launchChrome(){
        // Used to open the browser
        WebDriverManager.chromedriver().setup();
        System.setProperty("webdriver.chrome.driver", "D:\\QA\\testChrome\\114\\chromedriver-win64\\chromedriver.exe");

        // Declare and initialize ChromeOptions
        ChromeOptions options = new ChromeOptions();
        options.setBinary("D:\\QA\\testChrome\\114\\chrome-win64\\chrome.exe");

        // Used to direct to the web page
        webDriver = new ChromeDriver(options);
        webDriver.get("https://app.rolljak.com/users/sign_up");
        webDriver.manage().window().fullscreen();
        webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
    }

    //used to handle after the test
    @AfterMethod
    public void closeDriver(){
        if(webDriver != null){
            webDriver.close();
            System.out.println("Chrome Driver Closed");
        }
    }

    //used to do the test negative scenario
    @Test(description = "user should see error message with different invalid inputs")
    public void userCantRegisterWithInvalidCredentials(){

        //used to create an instance of the RegisterPage class to interact with the web page and perform registration actions
        RegisterPage registerPage = new RegisterPage(webDriver);

        //used to ensure the welcome message is displayed and has the expected text
        WebElement registerMessageElement = registerPage.getWelcomeRegisterMessage();
        assertWelcomeMessage(registerMessageElement);

        /**
         * Used to get invalid credential data
         */
        //used to get registered email
        String registeredEmail = "salman.magribie.qa@gmail.com";

        //used to get random valid password
        String randomValidPassword = TestDataProvider.getRandomPassword();

        //used to get random valid email
        String randomValidEmail = TestDataProvider.getRandomEmail();

        //used to get random invalid email
        String randomInvalidEmail = TestDataProvider.getRandomFirstName();

        //used to get random invalid password < 8 characters
        String randomInvalidPassword = TestDataProvider.getRandomInvalidPassword();

        /**
         * used to input invalid credential data and do registration
         */

        //User should be disable to register with blank email and password
        testBlankRegister(registerPage, "", "");

        //User should be disable to register with valid email but blank password
        testBlankRegister(registerPage, randomValidEmail, "");

        //User should be disable to register with blank email but valid password
        testBlankRegister(registerPage, "", randomValidPassword);

        //User should be disable to register with invalid email but valid password
        testInvalidEmailRegister(registerPage, randomInvalidEmail, randomValidPassword);

        //User should be disable to register with valid email but invalid password
        testInvalidPasswordRegister(registerPage, randomValidEmail, randomInvalidPassword);

        //User should be disable to register with already registered email and valid password
        testRegisteredEmailRegister(registerPage, registeredEmail, randomValidPassword);

    }
    //used to assert the welcome message
    private void assertWelcomeMessage(WebElement registerMessageElement) {
        Assert.assertTrue(registerMessageElement.isDisplayed(), "Register message is displayed");
        String expectedLoginMessage = "Let’s Start Rolling with Rolljak!";
        String actualLoginMessage = registerMessageElement.getText();
        Assert.assertEquals(actualLoginMessage, expectedLoginMessage, "Register message text is as expected");
    }
    //used to test and assert with blank credentials data when register
    private void testBlankRegister(RegisterPage registerPage, String email, String password){
        registerPage.clearRegistrationCredentials();
        registerPage.inputRegistrationCredentials(email, password);

        //retrieve the disable button element
        WebDriverWait wait = new WebDriverWait(webDriver, Duration.ofSeconds(5));
        WebElement disableSignUpButtonElement = registerPage.getSignUpButton();

        //used to assert the state of the disabled button
        Assert.assertTrue(disableSignUpButtonElement.isDisplayed(), "Disabled button is displayed");
    }

    //used to test and assert with invalid email when register
    private void testInvalidEmailRegister(RegisterPage registerPage,String email, String password){
        registerPage.clearRegistrationCredentials();
        registerPage.inputRegistrationCredentials(email, password);

        //retrieve the disable button element
        WebDriverWait wait = new WebDriverWait(webDriver, Duration.ofSeconds(5));
        WebElement errorMessageInvalidEmailMessage = registerPage.getErrorMessageInvalidEmailMessage();

        //used to assert that the error message is displayed and contains the expected text
        Assert.assertTrue(errorMessageInvalidEmailMessage.isDisplayed(), "Error message is displayed");
        String expectedErrorMessage = "Please enter a valid email";
        String actualErrorMessage = errorMessageInvalidEmailMessage.getText();
        Assert.assertEquals(actualErrorMessage, expectedErrorMessage, "Error message text is as expected");
    }

    //used to test and assert with invalid password when register
    private void testInvalidPasswordRegister(RegisterPage registerPage,String email, String password){
        registerPage.clearRegistrationCredentials();
        registerPage.inputRegistrationCredentials(email, password);

        //retrieve the disable button element
        WebDriverWait wait = new WebDriverWait(webDriver, Duration.ofSeconds(5));
        WebElement errorMessageInvalidPasswordMessage = registerPage.getErrorMessageInvalidPasswordMessage();

        //used to assert that the error message is displayed and contains the expected text
        Assert.assertTrue(errorMessageInvalidPasswordMessage.isDisplayed(), "Error message is displayed");
        String expectedErrorMessage = "At least 8 alphanumeric characters";
        String actualErrorMessage = errorMessageInvalidPasswordMessage.getText();
        Assert.assertEquals(actualErrorMessage, expectedErrorMessage, "Error message text is as expected");
    }

    private void testRegisteredEmailRegister(RegisterPage registerPage,String email, String password){
        registerPage.clearRegistrationCredentials();
        registerPage.inputRegistrationCredentials(email, password);
        registerPage.tapSignUp();

        //retrieve the disable button element
        WebDriverWait wait = new WebDriverWait(webDriver, Duration.ofSeconds(5));
        WebElement errorMessageRegisteredEmailMessage = registerPage.getErrorMessageRegisteredEmailMessage();

        //used to assert that the error message is displayed and contains the expected text
        Assert.assertTrue(errorMessageRegisteredEmailMessage.isDisplayed(), "Error message is displayed");
        String expectedErrorMessage = "has already been taken";
        String actualErrorMessage = errorMessageRegisteredEmailMessage.getText();
        Assert.assertEquals(actualErrorMessage, expectedErrorMessage, "Error message text is as expected");
    }

    //used to do the test bot should be disable to register with valid unregistered email and valid password caused protected on recaptcha
    @Test(description = "user should be able to register with valid email and valid password")
    public void userCanRegisterWithValidCredentials() {
        RegisterPage registerPage = new RegisterPage(webDriver);
        onBoardingPage onboardingPage = new onBoardingPage(webDriver);

        /**
         * Used to get valid credential data
         */
        String validEmail = TestDataProvider.getRandomEmail();
        String validPassword = TestDataProvider.getRandomPassword();

        /**
         * used to input valid credential data and do login
         */
        registerPage.inputRegistrationCredentials(validEmail, validPassword);
        registerPage.tapSignUp();

        // Assert that the user is successfully registered in to Rolljak user profile
        String currentUrl = webDriver.getCurrentUrl();
        Assert.assertTrue(currentUrl.contains("/users/profile"), "User is on the user profile after login");
    }


}
